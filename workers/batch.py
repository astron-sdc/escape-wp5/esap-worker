import json
import os
import re
import sys

import requests
from DIRAC.Core.Base import Script
from uws.classes import UWSJob
from uws.client import Client
from uws.workers import Worker

Script.initialize()
from DIRAC.Interfaces.API.Dirac import Dirac
from DIRAC.Interfaces.API.Job import Job


def get_filename_from_cd(cd):
    """
    Get filename from content-disposition
    """
    if not cd:
        return None
    fname = re.findall("filename=(.+)", cd)
    if len(fname) == 0:
        return None
    return fname[0]


def download_file(url, location):
    filename = os.path.basename(url)
    path = os.path.join(location, filename)
    if os.path.isfile(path):
        return path
    else:
        headers = {"User-Agent": "Firefox"}
        res = requests.get(url, stream=True, headers=headers)
        res.raise_for_status()
        with open(path, "wb") as local:
            # Chunked per MiB
            for chunk in res.iter_content(chunk_size=2**20):
                local.write(chunk)
        return path


class BatchSubmit(Worker):
    """A worker echoing all parameters"""

    def __init__(self):
        self._type = "batch.submit"

    def run(self, job: UWSJob, job_token: str, client: Client) -> None:
        dirac = Dirac()
        j = Job()
        data = {p.key: p.value for p in job.parameters}
        j.setCPUTime(data["cputime"])

        singularityImage = data["singularity_image"]
        executable = download_file(singularityImage, "/tmp/")

        diracPath = data["dirac_path"]
        diracSE = data["dirac_SE"]
        lfn_image = diracPath + os.path.basename(executable)

        if not dirac.getLfnMetadata(lfn_image, printOutput=False)["Value"][
            "Successful"
        ]:
            dirac.addFile(lfn_image, executable, diracSE, printOutput=False)

        j.setInputSandbox(["LFN:%s" % lfn_image])
        j.setOutputSandbox(["StdOut", "StdErr", data["output_file_name"]])

        j.setExecutable(f'/bin/bash -c "singularity run {lfn_image}"')
        j.setName(data["job_name"])
        j.setJobGroup(data["group_name"])

        jobID = dirac.submitJob(j)
        data = [{"key": "jobid", "value": jobID["JobID"]}]
        client.add_results(job.jobId, data, job_token)


class BatchQuery(Worker):
    """A worker echoing all parameters"""

    def __init__(self):
        self._type = "batch.query"

    def run(self, job: UWSJob, job_token: str, client: Client) -> None:
        dirac = Dirac()
        jobID = int({p.key: p.value for p in job.parameters}["jobid"])
        jobStatus = dirac.getJobStatus(jobID)
        if jobStatus["Value"] == {}:
            data = [{"key": "JobStatus", "value": "Not Found"}]
        else:
            data = [{"key": "JobStatus", "value": jobStatus["Value"][jobID]["Status"]}]
        client.add_results(job.jobId, data, job_token)
