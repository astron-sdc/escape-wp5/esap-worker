""" VO Query modules """

from pyvo.dal import DALQueryError, TAPService
from uws.classes import UWSJob
from uws.client import Client
from uws.workers import Worker


class VOTAPQueryWorker(Worker):
    def __init__(self):
        self._type = "query.vo.tap"

    def run(self, job: UWSJob, job_token: str, client: Client):

        try:
            params = {p.key: p.value for p in job.parameters}

            service_url = params["service"]
            query = params["query"]

            service = TAPService(service_url)
            # Submits the job (not started yet)
            tap_job = service.submit_job(query)

            # Run the job
            tap_job.run()

            # Depending on the server version, consists of
            # a busy loop or a long polling mechanism
            tap_job.wait()

            # Shortcut to raise an exception in case of
            # an error. Check `except DALQueryError as err:`
            # below
            tap_job.raise_if_error()

            client.add_results(
                job.jobId,
                [
                    {
                        "key": "result_uri",
                        "value": tap_job.result_uri,
                    }
                ],
                job_token,
            )

        except KeyError:
            client.fail_job(job.jobId, "Invalid parameters", job_token)
        except DALQueryError as err:
            client.fail_job(job.jobId, "Query error: " + err, job_token)
        except Exception as err:
            client.fail_job(job.jobId, "Uncaught Exception: " + err, job_token)
