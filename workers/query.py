from celery.utils.log import get_task_logger
from uws.classes import UWSJob
from uws.client import Client
from uws.workers import Worker

logger = get_task_logger(__name__)


class HttpQuery(Worker):
    """Worker performing a http query"""

    def __init__(self):
        self._type = "query.http"

    def run(self, job: UWSJob, job_token: str, client: Client) -> None:
        logger.info("Executing HTTP Query")

        # TODO: perform a real query!
        results = [{"key": "answer", "value": "42"}]

        client.add_results(job.job_id, results, job_token)


class Echo(Worker):
    """A worker echoing all parameters"""

    def __init__(self):
        self._type = "echo"

    def run(self, job: UWSJob, job_token: str, client: Client) -> None:
        data = [{"key": p.key, "value": p.value} for p in job.parameters]
        client.add_results(job.jobId, data, job_token)
