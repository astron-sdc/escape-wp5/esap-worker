from typing import Any, List

from celery.utils.log import get_task_logger
from uws.classes import UWSJob
from uws.client import Client
from uws.workers import Worker

logger = get_task_logger(__name__)


def extract_urls(shopping_basket: Any) -> List[str]:
    """Convert shopping basket entries to a list or URLs"""

    return []


class Orc(Worker):
    """Orchestration Worker"""

    def __init__(self):
        self._type = "orc.smash"

    def run(self, job: UWSJob, job_token: str, client: Client):
        parameters = {}
        for param in job.parameters:
            parameters[param.key] = param.value

        raise NotImplementedError("Orchestration is not yet implemented")

        # Required parameters(?)
        # - link to shopping basket
        # - link to software repository
        # - Binder service URL?

        # Extract URLs from Shopping Basket

        # Clone software directory

        # Construct postBuild
