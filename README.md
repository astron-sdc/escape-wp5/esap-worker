# ESAP Worker

ESAP Worker for orchestration of Data, Software and Infrastructure and performing asynchronous queries.

## Installation

This repository can use both `pip` and `pipenv` use either to install the necessary dependencies:
```bash
# Don't forget to activate your virtualenv!
pip install -r requirements.txt
# Or use pipenv:
pipenv install
```

## Running

```bash
# Make sure that `UWS_HOST` points to a valid UWS endpoint api (usually a local ESAP installation) and `CELERY_BROKER_URL`
# valid RabbitMQ instance

# Make sure the database and queue in ESAP API Gateway have been configured and migrated before running the worker
# otherwise it will complain about not able to connect to a Message Broker
celery -A esap_worker worker -l INFO
```

### Windows
Note on Windows you may need to add `--pool=solo` to make it work:
```bash
celery -A esap_worker worker --pool=solo -l INFO
```

### Debugging running a worker

The worker can be ran in debug mode in VSCode with the following `launch.json`:
```json
{
    "version": "0.2.0",
    "configurations": [
        {
            "name": "Python: Debug Worker",
            "type": "python",
            "request": "launch",
            "env": {
                "DJANGO_SETTINGS_MODULE": "esap_worker.settings"
            },
            "module": "celery",
            "args": [
                "-A",
                "esap_worker",
                "worker",
                "-l",
                "INFO"
            ],
            "cwd": "${workspaceFolder}/",
            "console": "integratedTerminal",
        }
    ]
}
```

Make sure to perform a request to trigger an invocation of your worker.


### DiRAC Version (experimental)

**NOTE** DiRAC support is experimental and does not work out of the box!

To use the DiRAC version of the worker, you will need to add the following mounts:
- `/root/.globus/` containing your certificates
- `/diracos/etc/` containing configuration settings provided by your DiRAC provider.

**NOTE** There is no support yet for initializing the proxy automatically or renewing the certificates!

Run with the following (added) environment variable:
```
-e ENV DJANGO_SETTINGS_MODULE=esap_worker.settings-dirac
```

### Debug version

In the Dockerfile, use the provided build argument `DEBUG` to add some extra tools in the image

## Adding worker types

To add a worker type, the following needs to be done:
* Create a class in a (existing) module in the `workers` directory.
* Make sure the new class inherits from the `uws.workers.Worker` class
* The class implements a `run(self, job: Job) -> None` method
* The class has a private `_type` property:
```python
    def __init__(self):
        self._type = "query.http"
```
* The worker type is listed in the `esap_worker.settings`:
```python
# Installed query workers
ESAP_WORKERS = ["workers.query.Http"]
```

## Contributing

For developer access, please post a message to the [ESAP](https://chat.escape2020.de/channel/esap) channel on rocket chat.
