FROM python:3.10

SHELL ["/bin/bash", "-c"]

RUN apt-get update
RUN apt-get install -y build-essential curl

ARG DEBUG
RUN if [[ -z "$DEBUG" ]] ;\
        then echo build regular build ;\
        else apt-get install -y wget git vim jq ;\
    fi

# DIRAC
WORKDIR /
RUN curl -LO https://github.com/DIRACGrid/DIRACOS2/releases/download/2.10/DIRACOS-Linux-x86_64.sh && bash DIRACOS-Linux-x86_64.sh
RUN source /diracos/diracosrc && pip3 install CTADIRAC

## ESAP Backend
ENV ESAPWORKER=/ESAPWORKER
RUN mkdir $ESAPWORKER
WORKDIR $ESAPWORKER

# For regular CI/CD workflow
COPY . .

RUN source /diracos/diracosrc && python3 -m pip install --upgrade pip -r requirements.txt

ENV CELERY_BROKER_URL=amqp://guest@host.docker.internal:5672
ENV UWS_HOST=http://host.docker.internal:5555/esap-api/uws/
ENV DJANGO_SETTINGS_MODULE=esap_worker.settings

CMD ["bash", "-c", "./start.sh"]
