from settings import *

# Installed query workers
UWS_WORKERS = ["workers.query.HttpQuery", "workers.orc.Orc", "workers.query.Echo", "workers.batch.BatchQuery", "workers.batch.BatchSubmit"]
